__author__ = 'ludo'

from flask import Blueprint, render_template

web_bp = Blueprint('web', __name__)

@web_bp.route('/')
def hello_world():
    return render_template("index.html", page_title="Door access control")
