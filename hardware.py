#-*- coding: utf-8 -*-
import threading

from gpio import GPIOController
from gpio import GPIOPinDirection as Direction
from gpio import GPIOPinEdge as Edge
from twisted.internet import reactor

__all__ = ['RgbLed', 'DoorTrigger', 'AccessControlManager']

GPIOController().available_pins = set()


class RgbLed:
    GPIO_RED = 2
    GPIO_GREEN = 1
    GPIO_BLUE = 4

    def __init__(self):
        self.gpio = GPIOController()
        self.gpio.available_pins |= {self.GPIO_RED, self.GPIO_GREEN, self.GPIO_BLUE}

        self.red_led = self.gpio.alloc_pin(self.GPIO_RED, Direction.OUTPUT)
        self.green_led = self.gpio.alloc_pin(self.GPIO_GREEN, Direction.OUTPUT)
        self.blue_led = self.gpio.alloc_pin(self.GPIO_BLUE, Direction.OUTPUT)

    def _set_led(self, led, value):
        if not value:
            led.set()
        else:
            led.reset()

    def set_rgb(self, r, g, b):
        self._set_led(self.red_led, r)
        self._set_led(self.green_led, g)
        self._set_led(self.blue_led, b)

    def get_rgb(self):
        return self.red_led.read(), self.green_led.read(), self.blue_led.read()


class DoorTrigger:
    GPIO_TRIGGER = 6
    OPEN_TIME = 0.5

    def __init__(self, mgr):
        self.gpio = GPIOController()
        self.gpio.available_pins |= {self.GPIO_TRIGGER}
        self.trigger = self.gpio.alloc_pin(self.GPIO_TRIGGER, Direction.OUTPUT)
        self.mgr = mgr

    def set(self, value):
        if value:
            self.trigger.set()
        else:
            self.trigger.reset()
        self.mgr.lock_state_changed(value)

    def get(self):
        return self.trigger.read()

    # Open and close OPEN_TIME seconds later
    def open_close(self):
        self.set(True)
        reactor.callLater(self.OPEN_TIME, self.set, False)

class DoorDetection:
    GPIO_DETECT = 5

    def __init__(self, mgr):
        self.gpio = GPIOController()
        self.gpio.available_pins |= {self.GPIO_DETECT}
        self.pin = self.gpio.alloc_pin(self.GPIO_DETECT, Direction.INPUT, mgr.open_state_changed, Edge.BOTH)


class AccessControlManager:
    CLOSED_LOCKED = 0
    CLOSED_UNLOCKED = 1
    OPENED = 2

    def __init__(self):
        self.doorTrigger = DoorTrigger(self)
        self.rgbLed = RgbLed()
        self.doorDetection = DoorDetection(self)
        self.state = self.CLOSED_LOCKED
        self.update_led()

    # state == 1 => dévérouillage de la porte
    def lock_state_changed(self, state):
        if state and self.state == self.CLOSED_LOCKED:
            self.state = self.CLOSED_UNLOCKED
        self.update_led()

    # state == 0 => ouverture de la porte
    def open_state_changed(self, number, state):
        if not state and self.state == self.CLOSED_UNLOCKED:
            self.state = self.OPENED
        elif state:
            self.state = self.CLOSED_LOCKED
        self.update_led()

    def update_led(self):
        if self.state == self.CLOSED_LOCKED:
            self.rgbLed.set_rgb(1, 0, 0)
        elif self.state == self.OPENED:
            self.rgbLed.set_rgb(0, 1, 0)
        elif self.state == self.CLOSED_UNLOCKED:
            self.rgbLed.set_rgb(1, 1, 0)
