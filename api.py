__author__ = 'ludo'

from flask import Blueprint
from flask.ext.restful import reqparse, Api, Resource

from hardware import *

manager = AccessControlManager()
api_bp = Blueprint('api', __name__)
api = Api(api_bp)

class RootApi(Resource):
    def get(self):
        return 'API'


class LedColorApi(Resource):
    def get(self):
        r, g, b = [int(not c) for c in manager.rgbLed.get_rgb()]
        return {'red': r, 'green': g, 'blue': b}

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('red', type=int)
        parser.add_argument('green', type=int)
        parser.add_argument('blue', type=int)
        args = parser.parse_args()
        manager.rgbLed.set_rgb(args.red, args.green, args.blue)
        return ('RGB colors changed %s %s %s' % (args.red, args.green, args.blue)), 201


class DoorOpenApi(Resource):
    def put(self):
        manager.doorTrigger.open_close()
        return 'Opening door for %d seconds' % DoorTrigger.OPEN_TIME

@api_bp.route('/')
def hello_world():
    return "Hello api"

api.add_resource(RootApi, '/')
api.add_resource(LedColorApi, '/led/color')
api.add_resource(DoorOpenApi, '/door/open')
