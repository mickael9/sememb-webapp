from flask import Flask
from werkzeug.debug import DebuggedApplication
from twisted.web import server
from twisted.web.wsgi import WSGIResource
from twisted.internet import reactor

from api import api_bp
from web import web_bp

app = Flask(__name__)
app.register_blueprint(api_bp, url_prefix='/api')
app.register_blueprint(web_bp, url_prefix='/')

if __name__ == '__main__':
    app.wsgi_app = DebuggedApplication(app.wsgi_app)
    wsgiAppAsResource = WSGIResource(reactor, reactor.getThreadPool(), app)
    reactor.listenTCP(80, server.Site(wsgiAppAsResource))
    reactor.run()
